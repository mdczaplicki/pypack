from zipfile import ZipFile, ZIP_DEFLATED
import os
import re
from PersGSMFinder import PersGSMFinder
from xml.etree import ElementTree

__author__ = 'mczaplic'


class DPPack:
    def __init__(self, customer_cfg):
        self.config_filename = "o:\\CPCEMEA\\TOOLS\\PyPack\\config.xml"
        self.error_message = ""
        self.warning_message = ""
        self.pers_export_dir, self.pers_cus_conf = PersGSMFinder.get_pgsm_path()

        # self.pers_customer_dir
        self.customer = dict(
            cfg=os.path.join(self.pers_export_dir, customer_cfg)
        )
        self.customer['trigram'] = self.__gettrigram__(self.customer['cfg'])
        self.customer['param'], self.customer['graph'] = \
            PersGSMFinder.get_customer_path(self.pers_cus_conf, self.customer['trigram'])
        self.customer['path'] = self.customer['param'][:self.customer['param'].index('param\\')]
        self.__checkgraphics__(self.customer['cfg'])

    def __gettrigram__(self, customer_cfg):
        """
        Collect customer trigram from *.CFG file.
        :return: string with trigram
        """
        try:
            template = re.template(r'[A-Z0-9][A-Z0-9][A-Z0-9][:]')
            cfg = open(customer_cfg, 'r')
            for line in cfg.readlines():
                if template.match(line):
                    cfg.close()
                    return line[:3]
        except Exception as e:
            raise Exception("Error in DPPack.__gettrigram__()\n%s" % e.message)

    def __checkgraphics__(self, customer_cfg):
        """
        If *.CFG can contain - we do not need to check its content.
        If it can't contain graphical, then we have to check if it is not present in *.CFG file.
        :return: None
        """
        tree = ElementTree.parse(open(self.config_filename, 'rb'))
        root = tree.getroot()
        try:
            graphical = root.find('graphical').text == "yes"
        except AttributeError:
            graphical = False

        if graphical:
            return None
        cfg = open(customer_cfg, 'r')
        if "graphical" in cfg.read().lower():
            self.error_message += "Exported config can't contain graphical profiles. \n"
            raise Exception("Exported config can't contain graphical profiles.")

    def list_to_pack(self, part):
        """
        Creates a list of files to be packed.
        :param part: cfg, cfg_source or module
        :return: a list of files
        """
        try:
            include_list, exclude_list = self.create_modification_list(part)

            pack_list = []
            for path, directories, files in os.walk(self.customer['param']):
                for f in files:
                    pack_list = self.filter_files_list(pack_list, f, path=path, include_list=include_list)
            new_pack_list = pack_list[:]
            for f in pack_list:
                new_pack_list = self.filter_files_list(new_pack_list, f, exclude_list=exclude_list)
            return_list = []
            for f in new_pack_list:
                if part == 'module' or part == 'cfg':
                    return_list.append(f.replace(self.customer['param'], ''))
                else:
                    return_list.append(f.replace(self.customer['path'], ''))
            if part == 'cfg':
                return_list.append(self.customer['cfg'].replace(self.pers_export_dir, ''))
            return return_list, new_pack_list
        except Exception as e:
            raise Exception("Error in DPPack.list_to_pack()\n%s" % e.message)

    def module_source(self, source_location):
        """
        Separate method because in this case we add all from targeted location. Later we just exclude files to be packed
        :param source_location: names for packed files, and path to files to be packed.
        :return:
        """
        try:
            _, exclude_list = self.create_modification_list('module_source')
            pack_list = []
            for path, directories, files in os.walk(source_location):
                for f in files:
                    file_path = os.path.join(path, f)
                    pack_list.append(file_path)
            new_pack_list = pack_list[:]
            for f in pack_list:
                new_pack_list = self.filter_files_list(new_pack_list, f, exclude_list=exclude_list)
            return_list = []
            for f in new_pack_list:
                return_list.append("MODULE" + f.replace(source_location, ''))
            return return_list, new_pack_list
        except Exception as e:
            raise Exception("Error in DPPack.module_source()\n%s" % e.message)

    def pack_module_source(self, name_list, pack_list, path=None, revision=None):
        """
        Takes an argument with list of files to be packed and pack them.
        :param name_list:
        :param pack_list:
        :param revision:
        :return:
        """
        try:
            zip_name = 'SOURCE_%s_DP_MODULE_[rev].zip' % self.customer['trigram']
            if revision:
                zip_name = zip_name.replace('[rev]', revision)
            if path:
                zip_name = os.path.join(path, zip_name)
            my_zip = ZipFile(zip_name, 'w', ZIP_DEFLATED)

            for f, name in zip(pack_list, name_list):
                my_zip.write(f, name)
            my_zip.close()
        except Exception as e:
            raise Exception("Error in DPPack.pack_module_source()\n%s" % e.message)

    def pack_from_list(self, part, pack_list, path=None, revision=None):
        """
        Takes an argument with list of files to be packed and pack them.
        :param part:
        :param pack_list:
        :return:
        """
        try:
            if part == 'cfg_source':
                zip_name = "SOURCE_%s_DP_CFG_[rev].zip" % self.customer['trigram']
            elif part == 'cfg':
                zip_name = "%s_DP_CFG_[rev].zip" % self.customer['trigram']
            elif part == 'module':
                zip_name = "%s_DP_MODULE_[rev].zip" % self.customer['trigram']
            else:
                self.error_message += "Wrong DP part. Expected: 'cfg_source', 'cfg', 'module'. Was: %s" % part
                raise Exception("DP part expected: cfg_source, cfg, module. Instead got: %s" % part)
            if revision:
                zip_name = zip_name.replace('[rev]', str(revision))
            if path:
                zip_name = os.path.join(path, zip_name)
            my_zip = ZipFile(zip_name, 'w', ZIP_DEFLATED)
            for f in pack_list:
                try:
                    if part == 'module' or part == 'cfg':
                        my_zip.write(f, f.replace(self.customer['param'], ''))
                    else:
                        my_zip.write(f, f.replace(self.customer['path'], ''))
                except Exception as e:
                    raise Exception("File problem, eg. modify date.\n%s" % e.message)
            if part == 'cfg':
                my_zip.write(self.customer['cfg'], self.customer['cfg'].replace(self.pers_export_dir, ''))
            my_zip.close()
        except Exception as e:
            raise Exception("Error in DPPack.pack_from_list()\n%s" % e.message)

    def filter_files_list(self, pack_list, f, path=None, include_list=None, exclude_list=None):
        """
        Include files to list or exclude them. Provide specifict list, never provide both list at once.
        :param pack_list: list with files
        :param f: files to be checked
        :param path: path to file (optional, because file itself can have path)
        :param include_list: list of include elements from config.xml
        :param exclude_list: list of exclude elements from config.xml
        :return:
        """
        try:
            if include_list is not None:
                file_extension = f[f.rfind('.') + 1:].lower()
                for element in include_list:
                    attribute, value = element['attribute'], element['value']
                    if attribute == "extension" and file_extension == value:
                        pack_list.append(os.path.join(path, f))
                    elif attribute == "name" and f.lower() == value:
                        pack_list.append(os.path.join(path, f))
                    elif attribute == "regex" and self.regex_match(value, f):
                        pack_list.append(os.path.join(path, f))
                return pack_list
            elif exclude_list is not None:
                path, file_name = os.path.split(f)
                file_extension = file_name[file_name.rfind('.') + 1:].lower()
                for element in exclude_list:
                    attribute, value = element['attribute'], element['value']
                    if attribute == "extension" and file_extension == value:
                        pack_list.remove(f) if f in pack_list else None
                        if "info" in element:
                            warning = self.component_warning(element['info'], element['component'])
                            self.add_warning(warning)
                    elif attribute == "name" and file_name.lower() == value:
                        pack_list.remove(f) if f in pack_list else None
                        if "info" in element:
                            warning = self.component_warning(element['info'], element['component'])
                            self.add_warning(warning)
                    elif attribute == "regex" and self.regex_match(value, file_name):
                        pack_list.remove(f) if f in pack_list else None
                        if "info" in element:
                            warning = self.component_warning(element['info'], element['component'])
                            self.add_warning(warning)
                    elif attribute == "folder" and value in path.lower():
                        pack_list.remove(f) if f in pack_list else None
                return pack_list
        except Exception as e:
            raise Exception("Error in DPPack.filter_files_list()\n%s" % e.message)

    def create_modification_list(self, zip_part):
        """
        Creates list with modifications for whiles which should be included or excluded from zip.
        :rtype : list, list
        :param zip_part: for exampe 'operation_elements', 'cfg', 'module, 'module_source'
        :return:
        """
        try:
            tree = ElementTree.parse(open(self.config_filename, 'rt'))
            root = tree.getroot().find('global')
            if root is None:
                self.error_message += "There is no 'global' tag in config.xml.\n"
                return None
            operation_elements = root.find(zip_part)
            include_list, exclude_list = [], []
            if operation_elements:
                for child in operation_elements:
                    if child.tag == "include":
                        include_list.append(dict(
                            value=child.text,
                            attribute=child.attrib['attribute']
                        ))
                    elif child.tag == "exclude":
                        exclude_list.append(dict(
                            value=child.text,
                            attribute=child.attrib['attribute']
                        ))
                        if "info" in child.attrib:
                            exclude_list[-1]['info'] = child.attrib['info']
                        if "component" in child.attrib:
                            exclude_list[-1]['component'] = child.attrib['component']
            else:
                self.warning_message += "No global operations for %s.\n" % zip_part
            root = tree.getroot().find(self.customer['trigram'].lower())
            if root:
                operation_elements = root.find(zip_part)
                if operation_elements:
                    for child in operation_elements:
                        if child.tag == "include":
                            include_list.append(dict(
                                value=child.text,
                                attribute=child.attrib['attribute']
                            ))
                        elif child.tag == "exclude":
                            exclude_list.append(dict(
                                value=child.text,
                                attribute=child.attrib['attribute']
                            ))
                # else:
                #     self.add_warning("No customer operations for %s.\n" % zip_part)
            return include_list, exclude_list
        except Exception as e:
            raise Exception("Error in DPPack.create_modification_list()\n%s" % e.message)

    def component_warning(self, info, component):
        """
        Creates warning message for adding PC to PA.
        :param info: PDM ref link
        :return: warning message
        """
        return "Add %s component (%s) to PA.\n" % (component, info)

    def add_warning(self, warning):
        """
        Checks if warning is in warning message list. If it is not, then add.
        :param warning: warning message
        :return: None
        """
        if warning not in self.warning_message:
            self.warning_message += warning

    @staticmethod
    def regex_match(pattern, sample):
        """
        Boolean value of matching a sample to a pattern.
        :param pattern: regex pattern
        :param sample: text to check
        :return: True or False
        """
        return bool(re.compile(pattern, re.IGNORECASE).match(sample))


class EPPack:
    def __init__(self):
        pass


if __name__ == "__main__":
    dp = DPPack("AML_20151012.CFG")
