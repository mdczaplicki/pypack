import ConfigParser
import os

__author__ = 'mczaplic'


class PersGSMFinder:
    def __init__(self):
        pass

    @staticmethod
    def get_pgsm_path():
        """
        Takes PersGSM folder from Gemplus/PersGSM/PGSM.INI file.
        :return: tuple with customer folder and export folder.
        """
        f = open('c:\\Gemplus\\PersGSM\\PGSM.INI', 'r+')

        # We shall skip first three lines to avoid crushing
        f.readline()
        f.readline()
        f.readline()

        config = ConfigParser.ConfigParser()
        config.readfp(f)
        path = config.get('DIRECTORY', 'CusConf')
        path = path.strip('\"')
        export = config.get('DIRECTORY', 'Export')
        export = export.strip('\"')

        return export, path

    @staticmethod
    def get_customer_path(cus_conf_path, trigram):
        file_name = "PGSM_" + trigram + ".INI"
        f = open(os.path.join(cus_conf_path, file_name), 'r')

        # We shall skip first three lines to avoid crushing
        f.readline()
        f.readline()
        f.readline()

        config = ConfigParser.ConfigParser()
        config.readfp(f)

        graph_path = config.get('DIRECTORY_' + trigram, 'Dir_Gra')
        graph_path = graph_path.strip('\"')

        param_path = config.get('DIRECTORY_' + trigram, 'Dir_Ext')
        param_path = param_path.strip('\"')

        return param_path, graph_path


