# -*- coding: utf-8 -*-
import os
import ConfigParser
from threading import Thread
from PyQt4 import QtCore, QtGui
from PyPack import DPPack
from PersGSMFinder import PersGSMFinder

__author__ = 'mczaplic'





def resource_path(relative_path):
    """
    Get absolute path to resource, works for dev and for PyInstaller
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


class MyWidgetTree(QtGui.QTreeWidget):
    def __init__(self, parent):
        super(MyWidgetTree, self).__init__(parent)
        self.isAllCollapsed = False
        self.isAllExpanded = True
        self.expandAll()

    def insertTopLevelItems(self, p_int, list_of_QTreeWidgetItem):
        super(MyWidgetTree, self).insertTopLevelItems(p_int, list_of_QTreeWidgetItem)
        self.expandAll()

    def expandAll(self):
        super(MyWidgetTree, self).expandAll()
        self.isAllCollapsed = False
        self.isAllExpanded = True

    def collapseAll(self):
        super(MyWidgetTree, self).collapseAll()
        self.isAllCollapsed = True
        self.isAllExpanded = False

    def mousePressEvent(self, e):
        assert isinstance(e, QtGui.QMouseEvent)
        if e.button() == 2:
            if self.isAllCollapsed:
                self.expandAll()
            else:
                self.collapseAll()
        else:
            super(MyWidgetTree, self).mousePressEvent(e)


class MyLineEdit(QtGui.QLineEdit):
    def __init__(self, parent):
        super(MyLineEdit, self).__init__(parent)

        self.setAcceptDrops(True)

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
            links = []
            for url in e.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.setText(''.join(links).replace('/', '\\'))
        else:
            e.ignore()

try:
    _from_utf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _from_utf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, d_a):
        return QtGui.QApplication.translate(context, text, d_a, _encoding)
except AttributeError:
    def _translate(context, text, d_a):
        return QtGui.QApplication.translate(context, text, d_a)


# noinspection PyCallByClass
class UiMainWindow(object):
    # thread pool
    thread_pool = [None, None, None, None]

    def __init__(self, main_window):
        self.dp = None
        self.prev_cfg_revision = ""
        self.prev_module_revision = ""
        self.prev_cfg_src_revision = ""
        self.prev_module_src_revision = ""
        self.cfg_pack_list = []
        self.module_pack_list = []
        self.cfg_src_pack_list = []
        self.module_src_pack_list = []
        self.module_src_show_list = []


        main_window.setObjectName(_from_utf8("main_window"))
        main_window.resize(760, 480)
        self.central_widget = QtGui.QWidget(main_window)
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(self.central_widget.sizePolicy().hasHeightForWidth())
        self.central_widget.setSizePolicy(size_policy)
        self.central_widget.setObjectName(_from_utf8("central_widget"))
        self.grid_layout = QtGui.QGridLayout(self.central_widget)
        self.grid_layout.setObjectName(_from_utf8("grid_layout"))

        self.config_combo_box = QtGui.QComboBox(self.central_widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.config_combo_box.setFont(font)
        self.config_combo_box.setToolTip(_from_utf8("Choose a customer config file"))
        self.config_combo_box.setObjectName(_from_utf8("config_combo_box"))
        self.grid_layout.addWidget(self.config_combo_box, 1, 0, 1, 2)

        self.cfg_horizontal = QtGui.QHBoxLayout()
        self.cfg_horizontal.setObjectName(_from_utf8("horizontal_layout"))
        self.grid_layout.addLayout(self.cfg_horizontal, 1, 2, 1, 1)

        self.cfg_revision_label = QtGui.QLabel()
        self.cfg_revision_label.setFont(font)
        self.cfg_revision_label.setObjectName(_from_utf8("cfg_revision_label"))
        self.cfg_revision_label.setText("CFG revision")
        self.cfg_revision_label.setAlignment(QtCore.Qt.AlignRight)
        self.cfg_horizontal.addWidget(self.cfg_revision_label, 1)

        self.cfg_revision_box = QtGui.QLineEdit(self.central_widget)
        self.cfg_revision_box.setFont(font)
        self.cfg_revision_box.setMaxLength(3)
        self.cfg_revision_box.setToolTip('Only letters are accepted')
        self.cfg_revision_box.setObjectName(_from_utf8("cfg_revision_box"))
        self.cfg_horizontal.addWidget(self.cfg_revision_box, 1)

        self.module_horizontal = QtGui.QHBoxLayout()
        self.module_horizontal.setObjectName(_from_utf8("horizontal_layout"))
        self.grid_layout.addLayout(self.module_horizontal, 1, 3, 1, 1)

        self.module_revision_label = QtGui.QLabel()
        self.module_revision_label.setFont(font)
        self.module_revision_label.setObjectName(_from_utf8("module_revision_label"))
        self.module_revision_label.setText("MODULE revision")
        self.module_revision_label.setAlignment(QtCore.Qt.AlignRight)
        self.module_horizontal.addWidget(self.module_revision_label, 1)

        self.module_revision_box = QtGui.QLineEdit(self.central_widget)
        self.module_revision_box.setFont(font)
        self.module_revision_box.setMaxLength(3)
        self.module_revision_box.setToolTip('Only letters are accepted')
        self.module_revision_box.setObjectName(_from_utf8("module_revision_box"))
        self.module_horizontal.addWidget(self.module_revision_box, 1)

        self.cfg_button = QtGui.QPushButton(self.central_widget)
        self.cfg_button.setFont(font)
        self.cfg_button.setObjectName(_from_utf8("cfg_button"))
        self.grid_layout.addWidget(self.cfg_button, 2, 0, 1, 1)
        self.cfg_src_button = QtGui.QPushButton(self.central_widget)
        self.cfg_src_button.setFont(font)
        self.cfg_src_button.setObjectName(_from_utf8("cfg_src_button"))
        self.grid_layout.addWidget(self.cfg_src_button, 2, 1, 1, 1)
        self.module_button = QtGui.QPushButton(self.central_widget)
        self.module_button.setFont(font)
        self.module_button.setObjectName(_from_utf8("module_button"))
        self.grid_layout.addWidget(self.module_button, 2, 2, 1, 1)
        self.module_src_button = QtGui.QPushButton(self.central_widget)
        self.module_src_button.setFont(font)
        self.module_src_button.setObjectName(_from_utf8("module_src_button"))
        self.grid_layout.addWidget(self.module_src_button, 2, 3, 1, 1)

        self.cfg_tree = MyWidgetTree(self.central_widget)
        self.cfg_tree.setObjectName(_from_utf8("cfg_tree"))
        self.cfg_tree.header().hide()
        self.cfg_tree.setStyleSheet("background-color: #CCC;")
        self.grid_layout.addWidget(self.cfg_tree, 3, 0, 1, 1)
        self.cfg_src_tree = MyWidgetTree(self.central_widget)
        self.cfg_src_tree.setObjectName(_from_utf8("cfg_src_tree"))
        self.cfg_src_tree.header().hide()
        self.cfg_src_tree.setStyleSheet("background-color: #CCC;")
        self.grid_layout.addWidget(self.cfg_src_tree, 3, 1, 1, 1)
        self.module_tree = MyWidgetTree(self.central_widget)
        self.module_tree.setObjectName(_from_utf8("module_tree"))
        self.module_tree.header().hide()
        self.module_tree.setStyleSheet("background-color: #CCC;")
        self.grid_layout.addWidget(self.module_tree, 3, 2, 1, 1)
        self.module_src_tree = MyWidgetTree(self.central_widget)
        self.module_src_tree.setObjectName(_from_utf8("module_src_tree"))
        self.module_src_tree.header().hide()
        self.module_src_tree.setStyleSheet("background-color: #CCC;")
        self.grid_layout.addWidget(self.module_src_tree, 3, 3, 1, 1)

        self.cfg_zip_button = QtGui.QPushButton(self.central_widget)
        self.cfg_zip_button.setFont(font)
        self.cfg_zip_button.setDisabled(True)
        self.cfg_zip_button.generated = False
        self.cfg_zip_button.setObjectName(_from_utf8("cfg_zip_button"))
        self.grid_layout.addWidget(self.cfg_zip_button, 4, 0, 1, 1)
        self.cfg_src_zip_button = QtGui.QPushButton(self.central_widget)
        self.cfg_src_zip_button.setFont(font)
        self.cfg_src_zip_button.setDisabled(True)
        self.cfg_src_zip_button.generated = False
        self.cfg_src_zip_button.setObjectName(_from_utf8("cfg_src_zip_button"))
        self.grid_layout.addWidget(self.cfg_src_zip_button, 4, 1, 1, 1)
        self.module_zip_button = QtGui.QPushButton(self.central_widget)
        self.module_zip_button.setFont(font)
        self.module_zip_button.setDisabled(True)
        self.module_zip_button.generated = False
        self.module_zip_button.setObjectName(_from_utf8("module_zip_button"))
        self.grid_layout.addWidget(self.module_zip_button, 4, 2, 1, 1)
        self.module_src_zip_button = QtGui.QPushButton(self.central_widget)
        self.module_src_zip_button.setFont(font)
        self.module_src_zip_button.setDisabled(True)
        self.module_src_zip_button.generated = False
        self.module_src_zip_button.setObjectName(_from_utf8("module_src_zip_button"))
        self.grid_layout.addWidget(self.module_src_zip_button, 4, 3, 1, 1)

        self.path_horizontal = QtGui.QHBoxLayout()
        self.path_horizontal.setObjectName(_from_utf8("path_horizontal"))
        self.path_box = MyLineEdit(self.central_widget)
        self.path_box.setToolTip(_from_utf8("This is where zips will be saved"))

        if os.path.isfile('path.ini'):
            config = ConfigParser.ConfigParser()
            config.read('path.ini')
            self.path_box.setText(config.get('path', 'project'))

        self.path_box.setObjectName(_from_utf8("path_box"))
        self.path_horizontal.addWidget(self.path_box)

        self.path_button = QtGui.QPushButton(self.central_widget)
        self.path_button.setObjectName(_from_utf8("path_button"))
        self.path_button.setToolTip(_from_utf8("Choose where to save zip files"))
        self.path_horizontal.addWidget(self.path_button)
        self.grid_layout.addLayout(self.path_horizontal, 5, 0, 1, 3)

        self.exit_button = QtGui.QPushButton(self.central_widget)
        self.exit_button.setFont(font)
        self.exit_button.setToolTip(_from_utf8("Quit the program"))
        self.exit_button.setObjectName(_from_utf8("exit_button"))

        self.grid_layout.addWidget(self.exit_button, 5, 3, 1, 1)
        main_window.setCentralWidget(self.central_widget)

        self.menu_bar = QtGui.QMenuBar(main_window)
        self.menu_bar.setGeometry(QtCore.QRect(0, 0, 760, 21))
        self.menu_bar.setObjectName(_from_utf8("menu_bar"))
        self.file_menu = QtGui.QMenu(self.menu_bar)
        self.file_menu.setObjectName(_from_utf8("file_menu"))
        self.help_menu = QtGui.QMenu(self.menu_bar)
        self.help_menu.setObjectName(_from_utf8("help_menu"))
        main_window.setMenuBar(self.menu_bar)
        self.refresh_action = QtGui.QAction(main_window)
        self.refresh_action.setObjectName(_from_utf8("refresh_action"))
        self.path_action = QtGui.QAction(main_window)
        self.path_action.setObjectName(_from_utf8("path_action"))
        self.exit_action = QtGui.QAction(main_window)
        self.exit_action.setObjectName(_from_utf8("exit_action"))
        self.how_action = QtGui.QAction(main_window)
        self.how_action.setObjectName(_from_utf8("how_action"))
        self.map_action = QtGui.QAction(main_window)
        self.map_action.setObjectName(_from_utf8("map_action"))
        self.about_action = QtGui.QAction(main_window)
        self.about_action.setObjectName(_from_utf8("about_action"))
        self.file_menu.addAction(self.refresh_action)
        self.file_menu.addAction(self.path_action)
        self.file_menu.addAction(self.exit_action)
        self.help_menu.addAction(self.how_action)
        self.help_menu.addAction(self.map_action)
        self.help_menu.addAction(self.about_action)
        self.menu_bar.addAction(self.file_menu.menuAction())
        self.menu_bar.addAction(self.help_menu.menuAction())

        self.retranslate_ui(main_window)
        QtCore.QObject.connect(self.exit_button, QtCore.SIGNAL(_from_utf8("clicked()")), main_window.close)
        QtCore.QObject.connect(self.cfg_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.list_files)
        QtCore.QObject.connect(self.cfg_zip_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.zip_files)

        QtCore.QObject.connect(self.cfg_src_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.list_files)
        QtCore.QObject.connect(self.cfg_src_zip_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.zip_files)

        QtCore.QObject.connect(self.module_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.list_files)
        QtCore.QObject.connect(self.module_zip_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.zip_files)

        QtCore.QObject.connect(self.module_src_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.list_files)
        QtCore.QObject.connect(self.module_src_zip_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.zip_files)

        QtCore.QObject.connect(self.path_button, QtCore.SIGNAL(_from_utf8("clicked()")), self.set_path)

        QtCore.QObject.connect(self.refresh_action, QtCore.SIGNAL(_from_utf8("triggered()")), self.refresh_config)
        QtCore.QObject.connect(self.path_action, QtCore.SIGNAL(_from_utf8("triggered()")), self.set_default_path)
        QtCore.QObject.connect(self.exit_action, QtCore.SIGNAL(_from_utf8("triggered()")), main_window.close)
        QtCore.QObject.connect(self.how_action, QtCore.SIGNAL(_from_utf8("triggered()")), self.show_how_to)
        QtCore.QObject.connect(self.map_action, QtCore.SIGNAL(_from_utf8("triggered()")), self.show_key_map)
        QtCore.QObject.connect(self.about_action, QtCore.SIGNAL(_from_utf8("triggered()")), self.show_about)

        QtCore.QObject.connect(self.cfg_revision_box, QtCore.SIGNAL(_from_utf8("editingFinished()")), self.exit_edit)
        QtCore.QObject.connect(self.module_revision_box, QtCore.SIGNAL(_from_utf8("editingFinished()")), self.exit_edit)

        QtCore.QObject.connect(self.config_combo_box, QtCore.SIGNAL(_from_utf8("currentIndexChanged(QString)")),
                               self.disable_zip_buttons)

        QtCore.QMetaObject.connectSlotsByName(main_window)

    def show_how_to(self):
        self.show_warning("1. Choose customers configuration from combo-box\n"
                          "\t1.1. If it is not on the list click: File/Refresh\n"
                          "2. Type CFG and MODULE revision\n"
                          "\t2.1. These can only be letters\n"
                          "\t2.2. Case insensitive - uppercased\n"
                          "3. Choose project folder at the bottom of the window\n"
                          "\t3.1. You can drag and drop folder onto text-box\n"
                          "4. Press buttons you need\n"
                          "\t4.1. If all ok - you can see files to be packed\n"
                          "5. Zip files with buttons below tree-view\n"
                          "\t5.1. If all ok, tree will become green.\n\n"
                          "Good Luck :)", "How to?")

    def refresh_config(self):
        try:
            self.config_combo_box.clear()
            export, _ = PersGSMFinder.get_pgsm_path()
            for path, _, files in os.walk(export):
                if path[-9:-1].lower() == "imexport":
                    for i, f in zip(xrange(len(files)), files):
                        if f.split('.')[-1] == "CFG":
                            self.config_combo_box.addItem(_from_utf8(f))
                            self.config_combo_box.setItemText(i, _translate("MainWindow", f, None))
                else:
                    break
        except Exception as e:
            self.show_warning("Error in refresh_config()\n%s" % e, "Error!")

    def show_key_map(self):
        self.show_warning('Ctrl+Q\t\tList CFG\n'
                          'Ctrl+W\t\tList CFG SRC\n'
                          'Ctrl+E\t\tList MODULE\n'
                          'Ctrl+R\t\tList MODULE SRC\n'
                          'Ctrl+Z\t\tSelect project folder\n'
                          'RMB on tree\tCollapse all/Expand all\n'
                          'ESC\t\tQuit the program', 'Key Map')

    def disable_zip_buttons(self):
        try:
            self.cfg_zip_button.setDisabled(True)
            self.cfg_zip_button.setText("DISABLED")
            self.cfg_zip_button.generated = False
            self.cfg_src_zip_button.setDisabled(True)
            self.cfg_src_zip_button.setText("DISABLED")
            self.cfg_src_zip_button.generated = False
            self.module_zip_button.setDisabled(True)
            self.module_zip_button.setText("DISABLED")
            self.module_zip_button.generated = False
            self.module_src_zip_button.setDisabled(True)
            self.module_src_zip_button.setText("DISABLED")
            self.module_src_zip_button.generated = False
        except Exception as e:
            self.show_warning("Error in disable_zip_buttons()\n%s" % e, "Error!")

    def show_about(self):
        msg_box = QtGui.QMessageBox(self.central_widget)
        msg_box.setWindowTitle("About")
        msg_box_icon = QtGui.QIcon()
        msg_box_icon.addFile(resource_path('mark.ico'))
        msg_box.setWindowIcon(msg_box_icon)
        msg_box.setText("Author: Marek Czaplicki\n"
                        "Organization: Gemalto Sp.z.o.o.")
        msg_box.setStyleSheet('background-color: black; color: orange;')
        msg_box.setIconPixmap(QtGui.QPixmap(resource_path('doge.jpeg')))
        msg_box.show()

    def exit_edit(self):
        try:
            sender = self.central_widget.sender()
            assert isinstance(sender, QtGui.QLineEdit)
            if str(sender.text()).isalpha():
                sender.setText(str(sender.text()).upper())
                sender.setStyleSheet('background-color: #81F781')
            elif not str(sender.text()).strip():
                sender.setStyleSheet('')
                sender.setText('')
            else:
                sender.setStyleSheet('background-color: #F78181')
                msg_box = QtGui.QMessageBox(self.central_widget)
                msg_box.setWindowTitle("Warning")
                msg_box.setText("Please type only letters in revision.\nAll will be uppercased.")
                msg_box_icon = QtGui.QIcon()
                msg_box_icon.addFile(resource_path('mark.ico'))
                msg_box.setWindowIcon(msg_box_icon)
                msg_box.show()
            if sender.objectName() == "cfg_revision_box":
                if self.prev_cfg_revision != str(sender.text()) and self.cfg_zip_button.text() != 'DISABLED':
                    self.cfg_zip_button.setDisabled(True)
                elif self.cfg_zip_button.text() != 'DISABLED' and not self.cfg_zip_button.generated:
                    self.cfg_zip_button.setEnabled(True)
                if self.prev_cfg_src_revision != str(sender.text()) and self.cfg_src_zip_button.text() != 'DISABLED':
                    self.cfg_src_zip_button.setDisabled(True)
                elif self.cfg_src_zip_button.text() != 'DISABLED' and not self.cfg_src_zip_button.generated:
                    self.cfg_src_zip_button.setEnabled(True)
            elif sender.objectName() == "module_revision_box":
                if self.prev_module_revision != str(sender.text()) and self.module_zip_button.text() != 'DISABLED':
                    self.module_zip_button.setDisabled(True)
                elif self.module_zip_button.text() != 'DISABLED' and not self.module_zip_button.generated:
                    self.module_zip_button.setEnabled(True)
                if self.prev_module_src_revision != str(sender.text()) and self.module_src_zip_button.text() != 'DISABLED':
                    self.module_src_zip_button.setDisabled(True)
                elif self.module_src_zip_button.text() != 'DISABLED' and not self.module_src_zip_button.generated:
                    self.module_src_zip_button.setEnabled(True)
        except Exception as e:
            self.show_warning("Error in exit_edit()\n%s" % e, "Error!")

    def set_default_path(self):
        try:
            path = str(QtGui.QFileDialog.getExistingDirectory(None, "Select default path", "C:\\"))
            if path:
                config = ConfigParser.ConfigParser(0)
                fp = open('path.ini', 'w+')
                config.add_section('path')
                config.set('path', 'project', path)
                config.write(fp)
                fp.close()
                self.path_box.setText(path)
        except Exception as e:
            self.show_warning("Error in set_default_path()\n%s" % e, "Error!")

    def set_path(self):
        try:
            path = QtGui.QFileDialog.getExistingDirectory(None, "Select path for zip",
                                                          str(self.path_box.text()))
            if path:
                self.path_box.setText(path)
        except Exception as e:
            self.show_warning("Error in set_path()\n%s" % e, "Error!")

    def list_files(self):
        try:
            sender = self.central_widget.sender()
            assert isinstance(sender, QtGui.QPushButton)
            cfg = str(self.config_combo_box.currentText())
            self.dp = DPPack(cfg)
            if sender.text() == "CFG files":
                cfg_revision = str(self.cfg_revision_box.text())
                if not cfg_revision:
                    return self.show_warning("CFG revision missing.")

                self.cfg_tree.clear()
                show_list, self.cfg_pack_list = self.dp.list_to_pack('cfg')
                items = self.tree_widget_list(show_list)
                self.cfg_tree.insertTopLevelItems(0, items)
                self.cfg_zip_button.setText("%s_DP_CFG_%s.zip" % (self.dp.customer['trigram'], cfg_revision))
                self.cfg_zip_button.setEnabled(True)
                self.cfg_zip_button.generated = False
                self.prev_cfg_revision = str(self.cfg_revision_box.text())
                self.cfg_tree.setStyleSheet("background-color: #CCC;")
            elif sender.text() == "CFG SRC files":
                cfg_revision = str(self.cfg_revision_box.text())
                if not cfg_revision:
                    return self.show_warning("CFG revision missing.")

                self.cfg_src_tree.clear()
                show_list, self.cfg_src_pack_list = self.dp.list_to_pack('cfg_source')
                items = self.tree_widget_list(show_list)
                self.cfg_src_tree.insertTopLevelItems(0, items)
                self.cfg_src_zip_button.setText("SOURCE_%s_DP_CFG_%s.zip" % (self.dp.customer['trigram'], cfg_revision))
                self.cfg_src_zip_button.setEnabled(True)
                self.cfg_src_zip_button.generated = False
                self.prev_cfg_src_revision = str(self.cfg_revision_box.text())
                self.cfg_src_tree.setStyleSheet("background-color: #CCC;")
            elif sender.text() == "MODULE files":
                module_revision = str(self.module_revision_box.text())
                if not module_revision:
                    return self.show_warning("MODULE revision missing.")

                self.module_tree.clear()
                show_list, self.module_pack_list = self.dp.list_to_pack('module')
                items = self.tree_widget_list(show_list)
                self.module_tree.insertTopLevelItems(0, items)
                self.module_zip_button.setText("%s_DP_MODULE_%s.zip" % (self.dp.customer['trigram'], module_revision))
                self.module_zip_button.setEnabled(True)
                self.module_zip_button.generated = False
                self.prev_module_revision = str(self.module_revision_box.text())
                self.module_tree.setStyleSheet("background-color: #CCC;")
            elif sender.text() == "MODULE SRC files":
                module_revision = str(self.module_revision_box.text())
                if not module_revision:
                    return self.show_warning("MODULE revision missing.")

                module_src_path = str(QtGui.QFileDialog.getExistingDirectory(None, "Choose location of MODULE SOURCE",
                                                                             str(self.path_box.text())))
                if not module_src_path:
                    return
                self.module_src_tree.clear()
                self.module_src_show_list, self.module_src_pack_list = self.dp.module_source(module_src_path)
                items = self.tree_widget_list(self.module_src_show_list)
                self.module_src_tree.insertTopLevelItems(0, items)
                self.module_src_zip_button.setText("SOURCE_%s_DP_MODULE_%s.zip" % (self.dp.customer['trigram'],
                                                                                   module_revision))
                self.module_src_zip_button.setEnabled(True)
                self.module_src_zip_button.generated = False
                self.prev_module_src_revision = str(self.module_revision_box.text())
                self.module_src_tree.setStyleSheet("background-color: #CCC;")
            if self.dp.warning_message:
                self.show_warning(self.dp.warning_message)
            del self.dp
        except Exception as e:
            self.show_warning("Error in list_files()\n%s" % e, "Error!")

    class Temp(QtCore.QObject):
        finished = QtCore.pyqtSignal()
        error = QtCore.pyqtSignal()

        def __init__(self, gui, module_revision):
            QtCore.QObject.__init__(self)
            self.gui = gui
            self.module_revision = module_revision

        def run(self):
            try:
                self.gui.dp.pack_module_source(self.gui.module_src_show_list, self.gui.module_src_pack_list,
                                               path=str(self.gui.path_box.text()), revision=self.module_revision)
                self.finished.emit()
            except Exception as e:
                self.error.emit()
                raise e

    def zip_files(self):
        try:
            sender = self.central_widget.sender()
            assert isinstance(sender, QtGui.QPushButton)
            cfg = str(self.config_combo_box.currentText())
            self.dp = DPPack(cfg)

            def disable():
                sender.setDisabled(True)
                sender.generated = True

            if sender.objectName() == "cfg_zip_button":
                try:
                    cfg_revision = str(self.cfg_revision_box.text())
                    self.dp.pack_from_list('cfg', self.cfg_pack_list,
                                           path=str(self.path_box.text()),
                                           revision=cfg_revision)
                    self.cfg_tree.setStyleSheet('background-color: #81F781')
                except Exception as e:
                    self.cfg_tree.setStyleSheet('background-color: #F78181')
                    raise e

            elif sender.objectName() == "cfg_src_zip_button":
                try:
                    cfg_revision = str(self.cfg_revision_box.text())
                    self.dp.pack_from_list('cfg_source', self.cfg_src_pack_list,
                                           path=str(self.path_box.text()), revision=cfg_revision)
                    self.cfg_src_tree.setStyleSheet('background-color: #81F781')
                except Exception as e:
                    self.cfg_src_tree.setStyleSheet('background-color: #F78181')
                    raise e

            elif sender.objectName() == "module_zip_button":
                try:
                    if not self.module_pack_list:
                        self.show_warning("There are no files to be packed.")
                        return disable()
                    module_revision = str(self.module_revision_box.text())
                    self.dp.pack_from_list('module', self.module_pack_list,
                                           path=str(self.path_box.text()), revision=module_revision)
                    self.module_tree.setStyleSheet('background-color: #81F781')
                except Exception as e:
                    self.module_tree.setStyleSheet('background-color: #F78181')
                    raise e
            elif sender.objectName() == "module_src_zip_button":
                self.module_src_tree.setStyleSheet('background-color: #ffff66')
                if not self.module_src_pack_list:
                    self.show_warning("There are no files to be packed.")
                    return disable()
                module_revision = str(self.module_revision_box.text())

                self.thread_pool[3] = None
                self.thread_pool[3] = QtCore.QThread()

                def ok():
                    self.module_src_tree.setStyleSheet('background-color: #81F781')
                    self.thread_pool[3].quit()

                def err():
                    self.module_src_tree.setStyleSheet('background-color: #F78181')
                    self.thread_pool[3].quit()

                tmp = self.Temp(self, module_revision)
                tmp.moveToThread(self.thread_pool[3])
                tmp.finished.connect(ok)
                tmp.error.connect(err)
                self.thread_pool[3].started.connect(tmp.run)
                self.thread_pool[3].start()
                # QtCore.QThreadPool().globalInstance().start(tmp)

                # try:
                #     # self.dp.pack_module_source(self.module_src_show_list, self.module_src_pack_list,
                #     #                            path=str(self.path_box.text()), revision=module_revision)
                #     self.module_src_tree.setStyleSheet('background-color: #81F781')
                # except Exception as e:
                #     self.module_src_tree.setStyleSheet('background-color: #F78181')
                #     raise e
            return disable()
        except Exception as e:
            self.show_warning("Error in zip_files()\n%s" % e, "Error!")

    def show_warning(self, warning_message, title="Warning!"):
        msg_box = QtGui.QMessageBox(self.central_widget)
        msg_box.setText(warning_message)
        msg_box.setWindowTitle(title)
        msg_box_icon = QtGui.QIcon()
        msg_box_icon.addFile(resource_path('mark.ico'))
        msg_box.setIconPixmap(QtGui.QPixmap(resource_path('pixel.ico')))
        msg_box.setWindowIcon(msg_box_icon)
        msg_box.setStyleSheet('messagebox-text-interaction-flags: 5;')
        msg_box.show()

    def tree_widget_list(self, show_list):
        """
        Creates a list for updating tree widget
        :param show_list:
        :return:
        """
        try:
            items = []
            for item in show_list:
                item_parts = item.split('\\')

                entry = QtGui.QTreeWidgetItem(None, [item_parts[0]])
                items_text = [i.text(0) for i in items]
                if entry.text(0) not in items_text:
                    parent_item = entry
                else:
                    parent_index = items_text.index(entry.text(0))
                    parent_item = items[parent_index]

                if len(item_parts) > 1:
                    for i in item_parts[1:]:
                        child_item = QtGui.QTreeWidgetItem(None, [i])
                        child_list_text = [parent_item.child(i).text(0) for i in xrange(parent_item.childCount())]
                        if child_item.text(0) in child_list_text:
                            child_index = child_list_text.index(child_item.text(0))
                            parent_item = parent_item.child(child_index)
                        else:
                            parent_item.addChild(child_item)
                            parent_item = child_item
                items.append(entry) if entry.text(0) not in items_text else None
            return items
        except Exception as e:
            self.show_warning("Error in tree_widget_list()\n%s" % e, "Error!")

    def retranslate_ui(self, main_window):
        main_window.setWindowTitle(_translate("main_window", "PyPack", None))
        export, _ = PersGSMFinder.get_pgsm_path()
        for path, _, files in os.walk(export):
            if path[-9:-1].lower() == "imexport":
                for i, f in zip(xrange(len(files)), files):
                    if f.split('.')[-1] == "CFG":
                        self.config_combo_box.addItem(_from_utf8(f))
                        self.config_combo_box.setItemText(i, _translate("MainWindow", f, None))
            else:
                break
        self.exit_button.setText(_translate("main_window", "Quit PyPack", None))
        self.exit_button.setShortcut(_translate("main_window", "Esc", None))
        self.cfg_button.setText(_translate("main_window", "CFG files", None))
        self.cfg_button.setShortcut(_translate("main_window", "Ctrl+Q", None))
        self.module_button.setText(_translate("main_window", "MODULE files", None))
        self.module_button.setShortcut(_translate("main_window", "Ctrl+E", None))
        self.module_src_button.setText(_translate("main_window", "MODULE SRC files", None))
        self.module_src_button.setShortcut(_translate("main_window", "Ctrl+R", None))
        self.cfg_src_button.setText(_translate("main_window", "CFG SRC files", None))
        self.cfg_src_button.setShortcut(_translate("main_window", "Ctrl+W", None))
        self.cfg_zip_button.setText(_translate("main_window", "DISABLED", None))
        self.cfg_zip_button.setShortcut(_translate("main_window", "Ctrl+A", None))
        self.cfg_src_zip_button.setText(_translate("main_window", "DISABLED", None))
        self.cfg_src_zip_button.setShortcut(_translate("main_window", "Ctrl+S", None))
        self.module_zip_button.setText(_translate("main_window", "DISABLED", None))
        self.module_zip_button.setShortcut(_translate("main_window", "Ctrl+D", None))
        self.module_src_zip_button.setText(_translate("main_window", "DISABLED", None))
        self.module_src_zip_button.setShortcut(_translate("main_window", "Ctrl+F", None))
        self.path_button.setText(_translate("main_window", "...", None))
        self.path_button.setShortcut(_translate("main_window", "Ctrl+Z", None))
        self.file_menu.setTitle(_translate("main_window", "File", None))
        self.help_menu.setTitle(_translate("main_window", "Help", None))
        self.refresh_action.setText(_translate("main_window", "Refresh", None))
        self.path_action.setText(_translate("main_window", "Select default path", None))
        self.how_action.setText(_translate("main_window", "How to?", None))
        self.map_action.setText(_translate("main_window", "Key map", None))
        self.about_action.setText(_translate("main_window", "About", None))
        self.exit_action.setText(_translate("main_window", "Exit", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    main_window_global = QtGui.QMainWindow()
    ui = UiMainWindow(main_window_global)
    app_ico = QtGui.QIcon()
    app_ico.addFile(resource_path('main.ico'))
    app.setApplicationName('PyPack')
    app.setOrganizationName('Gemalto')
    app.setOrganizationDomain('http://gemalto.com/')
    main_window_global.setWindowIcon(app_ico)
    main_window_global.show()
    sys.exit(app.exec_())

